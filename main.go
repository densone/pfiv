package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
	"time"
)

func main() {
	//load test email file. let's pretend this is an incoming pipe
	file, err := os.Open("data/test_email.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	data := make(map[string]interface{})

	//do something more posix friendly here.
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		parseLine(scanner.Text(), &data)
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	jsonData, err := json.MarshalIndent(data, "", "\t")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Here's the JSON DATA")
	println("<---------------------------------------------------->")
	fmt.Println(string(jsonData))
	po := &PlannedOutage{}
	err = json.Unmarshal(jsonData, po)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("\n\n\nHere's the Struct DATA")
	println("<---------------------------------------------------->")
	fmt.Printf("Message from %s\n%s\n\nReference: %s\nCC: %s\nStart: %s\nEnd: %s\n", po.NocEmail, po.Subject, po.RefNum, po.ServiceID, po.StartDateTime, po.EndDateTime)
	time.Sleep(2 * time.Second)

}

//evaluate each line and try to assign to a map. This is simple and crude but seems to work pretty well.
func parseLine(line string, data *map[string]interface{}) {
	fields := map[string]string{
		"Subject":       "Subject:",
		"Customer":      "Customer:",
		"RefNum":        "PW Reference number:",
		"StartDateTime": "Start Date and Time:",
		"EndDateTime":   "End Date and Time:",
		"ActionReason":  "Action and Reason:",
		"Location":      "Location of work:",
		"ServiceID":     "Service ID:",
		"Impact":        "Impact:",
		"NocEmail":      "E-mail:",
	}

	for field, fieldstr := range fields {
		if strings.Contains(line, fieldstr) {
			//Trim the field values
			output := strings.TrimLeft(line, fieldstr)
			output = strings.TrimRight(output, "\n")
			//trim prefix and suffix spaces.
			output = strings.TrimLeft(output, " ")
			output = strings.TrimRight(output, " ")
			(*data)[field] = output
		}
	}
}
