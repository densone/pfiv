package main

import (
	"strings"
	"time"
)

//primary data structure for planned outage
type PlannedOutage struct {
	Subject       string
	Customer      string
	RefNum        string
	StartDateTime PFTime
	EndDateTime   PFTime
	ActionReason  string
	Location      string
	ServiceID     string
	Impact        string
	NocEmail      string
}

//custom time for custom format from email
type PFTime struct {
	time.Time
}

//unmarshal custom time format and parse it.
func (pf *PFTime) UnmarshalJSON(input []byte) error {
	strInput := string(input)
	strInput = strings.Trim(strInput, `"`)
	newTime, err := time.Parse("2006-Jan-02 15:04 MST", strInput)
	if err != nil {
		return err
	}

	pf.Time = newTime
	return nil
}
