# NOC Email Handling Architecture
![image](https://gitlab.com/densone/pfiv/raw/master/design/diagram.png)

## Design Proposal

- Emails from various network providers will be sent to a catch address. This address will be checked by a customer service tool like zendesk. When a new noc related email is receiving zendesk will classify that emailed.   

- The NOC processor daemon will be constantly watching for emails that will create a potential outage or service degredation. When an email is identified by the noc processor several steps occur.  

	1. The NOC Proccessor sets planned downtime against various monitoring services: `Status Page, Nagios, PagerDuty`

	2. The NOC Processor gathers customer data about the circuit that will experience an outage.  

	3. SLA Files will be parsed and customers will be notified of the changes.  

	4. In house stakeholders will receive a slack message of the pending outage.  

	5. Customers have the ability to response to the email received and ask for more information.         
	6. The NOC Processor will keep a record of planned outages and verify these outages from within the log data. Once the log data is identified a slack message will be sent to the Network Reliability channel, just alerting folks of the maintenance. 

## The NOC Processor Design 

- The noc processor is written in Golang for it's portability and limited dependence on other packagesand tools.   

- The noc processor will use an internal key/value store that is regularly backed up. 

- design caveats - The noc processor will need an interface that can parse emails from 100s of different providers. There are many issues that can arise from within these emails including the following: 
	
	1. Time format. As simple as time can be, it can be 10x harder to deal with. We need to be able to effeciently parse different time formats.  

	2. Changes. We need to be able to rapidly modify inbound email template parses for when the NOCS modify their data formats.  

	3. We need a smart model for when NOC emails are rejected and we can manually repair and add more templating. 

	4. We need a model for CRUD operations. When an outage window changes, we need to be able to update the internal key/value store and trigger all changes to the customer. CRUD operations will work based on whther there was an initial record for the planned outage. If there is a record in the key/value store, all systems will be updated and customers will be alerted. 
 
## Matching against existing outages 
The noc processor will identify planned outages against untimely parallel outages by verifying the circuit ID in the logs. If an outage appears that is related do a different circuit id, it will be treated as a new outage.  

## Conclusion 
All in all this is a simple application design that can have serious impact if done correctly. Some of the points are high level, so please feel free to ask me to dig deeper if you feel like there's a flaw.  

# Simple Email Parser
This is a simple example of taking an email text file and grabbing the relevant fields to make an actionable decision. 

This does not work with multiple sources with different layouts. This will be a future goal so an interface for each email type will have to be created. 

## files
- `main.go` - Is the guts of the program
- `data.go` - Store the struct and the custom time unmarshaler

## How to Run the Program 

inside the directory execute

`go run *.go`


## What happens?

- The file is openened into a stream. 
- Each line is checked against a map
- The values are trimmed 
- A new map is generated and marshaled to json  
- The json map is dumped into a struct


